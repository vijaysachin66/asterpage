# Aster - Landing Page

## Development Setup

- Clone Repository: `git clone HTTPS/SSH Url`
- Move to root directory: `cd asterpage`
- Running command: `http-server`

## Tools & Package

* Visual Studio Code
* Bootstrap
* Jquery
* Slick Carousel
* Live Sass Compiler (Visual Studio code Extension)


## Testing Server

[Start Testing](http://asterpage.s3-website-us-east-1.amazonaws.com/)

## Developer Best Practice

- Maintain proper namespacing for folders, files, variable and function declarations.
- Always create feature or bug branches and then merge with stable master branch.
- Provide proper commit messages & split commits meaningfully.
