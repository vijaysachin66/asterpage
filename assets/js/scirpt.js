$(document).ready(function () {
  // faq
  if ($('.panel-collapse').hasClass('out')) {
    $('.plus').removeClass('hide');
  }
  $('.panel-heading').click(function () {
    $(this).find('.plus').toggleClass('hide');
  });

  //   testimonail
  $('.testimonail__section__cards').slick({
    infinite: false,
    arrows: true,
    dots: false,
    autoplay: true,
    speed: 1000,
    slidesToShow: 2.5,
    slidesToScroll: 1,
    nextArrow:
      '<div class="slick-right"><img src="./assets/images/right.png"></div>',
    prevArrow:
      '<div class="slick-left"><img src="./assets/images/left.png"></div>',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 1.2,
          slidesToScroll: 1
        }
      }
    ]
  });

  //   Append
  // doctor section
  var $doctorDiv = $('#doctor');
  for (i = 0; i < 4; i++) {
    $doctorDiv.append(
      '<div class="col-md-6"><div class="doctor__section--card"><div class="doctor__section--card--details"><h2>Pediatric BMT</h2><img src="./assets/images/person.png" /><h3>Dr. Vijay Agarwal</h3><label>MD, MRCP, PhD,CCT<br />Lead & Sr. Consultant - Medical Oncology & Haematology</label><p> Dr. Vijay Agarwal is a Senior Consultant Medical Oncologist with over 12 years of experience in Oncology. Dr. Agarwal has been practising Medical Oncology since 2004.</p><a class="red-btn gradient">Know More</a></div></div></div>'
    );
  }
});
